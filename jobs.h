#pragma once
#include <iostream>
#include <string>


using namespace std;
class jobs
{
private:
	string jobName;
	int arrival;
	int duration;
	
public:
	jobs();
	
	void setJobName(string n);
	string getJobName()const;

	void setArrival(int a);
	int getArrival()const;

	void setDuration(int d);
	int getDuration()const;
	~jobs();
};

