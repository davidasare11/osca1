#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <queue>
#include <vector>
#include "jobs.cpp"

using namespace std;

void readFile(vector<jobs>& jobVector) {
	ifstream inFile("test.txt", ios::in);
	string line;
	int i = 0;

	string jobName;
	int arrival, duration;

	if (inFile.is_open())
	{
		getline(inFile, line);
		inFile >> jobName >> arrival >> duration;

		jobs job;
		job.setJobName(jobName);
		job.setArrival(arrival);
		job.setDuration(duration);
		jobVector.push_back(job);
		i++;

		while (inFile.good() && !inFile.eof()) {
			getline(inFile, line);

			inFile >> jobName >> arrival >> duration;
			if (inFile.eof()) return; 

			jobs job2;
			job2.setJobName(jobName);
			job2.setArrival(arrival);
			job2.setDuration(duration);
			jobVector.push_back(job2);
			i++;

		}
	}
	else {
		cout << "Problem opening file" << endl;
	}
	inFile.close();
}

void printFileVector(const vector<jobs>& myVector) {
	cout << "Print file vector" << endl;
	cout << "--------------------------------------------" << endl;
	cout << "JOB_NAME\tARRIVAL_TIME\tDURATION" << endl;
	cout << "--------------------------------------------" << endl;
	unsigned int size = myVector.size();
	for (unsigned int i = 0; i < size; i++) {
		cout << myVector[i].getJobName() << "\t\t" << myVector[i].getArrival() << "\t\t" << myVector[i].getDuration() << endl;
	}
}

void printFileVector(const vector<jobs>& myVector) {
	cout << "Print file vector" << endl;
	cout << "--------------------------------------------" << endl;
	cout << "Job Name\tArrival Time\t\tDuration" << endl;
	cout << "--------------------------------------------" << endl;
	unsigned int size = myVector.size();
	for (unsigned int i = 0; i < size; i++) {
		cout << myVector[i].getJobName() << "\t\t" << myVector[i].getArrival() << "\t\t" << myVector[i].getDuration() << endl;
	}
}

void printOutput(vector<jobs> job) {
	int totalTime = 0;
	int loop = 0;
	int index = 0;

	cout << "\tFIFO" << endl;
	for (int i = 0; i < totalTime; i++)
	{
		if (i == job[loop].getArrival())
		{
			cout << "Arrived:" << job[loop].getJobName() << endl;
			loop++;
		}

		int count = 0;
		string fifo;

		int	durationSize = job[index].getDuration();
		if (count < job[index].getDuration()) {
			fifo = job[index].getDuration();
			count++;
			if (count == (durationSize))
			{
				index++;
				count = 0;
			}
		}
	}
}

int main() {
	vector<jobs> Job;
	readFile(Job);
	printOutput(Job);
	
	return 0;
}


