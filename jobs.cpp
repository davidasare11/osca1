#include "stdafx.h"
#include "jobs.h"


jobs::jobs()
{
	string jobName = "";
	int arrival = 0;
	int duration = 0;
}


void jobs::setJobName(string n)
{
	jobName = n;
}

string jobs::getJobName() const
{
	return jobName;
}

void jobs::setArrival(int a)
{
	arrival = a;
}

int jobs::getArrival() const
{
	return arrival;
}

void jobs::setDuration(int d)
{
	duration = d;
}

int jobs::getDuration() const
{
	return duration;
}


jobs::~jobs()
{
}
